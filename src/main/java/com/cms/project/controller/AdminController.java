package com.cms.project.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms.project.Entity.Admin;
import com.cms.project.Entity.Faculity;
import com.cms.project.reposistry.AdminReposistry;

@RestController
@CrossOrigin("http://localhost:4200")
public class AdminController {
	
	private AdminReposistry adr;
	
	@Autowired
	public void f1(AdminReposistry y)
	{ 
		System.out.println("AdminReposistry autowired successfully");
		adr =y;
		
	}

	@PostMapping("/adminLogin")
	public Admin login(@RequestBody Admin x)
	{
		Admin admin = new Admin();
		
		Optional<Admin> oid= adr.findById(x.getUserid());
		 if(oid.isPresent())
		 {
			 admin = oid.get();
			 
			 if(admin.getPassword().compareTo(x.getPassword())==0)
			 {
				 System.out.println(admin.getUserid()+" "+admin.getPassword());
				  
				 x = admin;
				
			 }
			
		 }
		
		return x;
	}
	
}
