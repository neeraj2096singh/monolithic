package com.cms.project.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cms.project.Entity.Attendmark;
import com.cms.project.Entity.Student;
import com.cms.project.reposistry.Atttendmarkreposistry;

@CrossOrigin("http://localhost:4200")
@RestController
public class AttendMarkController {

	private Atttendmarkreposistry atd;
	
	@Autowired
	public void f1(Atttendmarkreposistry y)
	{ 
		System.out.println("Atttendmarkreposistry autowired successfully");
		atd =y;
		
	}
	
	@PostMapping("/stdmark")
	public List<Attendmark> studentMark(@RequestBody Student s)
	{
		
		int x  = s.getStudentid();
		//Optional<Attendmark> at =  atd.findById(x);

			List<Attendmark> l = atd.findAll();
		
	
	     
		for(int i=0;i<l.size();i++)
		{
			System.out.println(l.get(i).getSubjectname()+" "+l.get(i).getMark());
		}
	
		 return atd.oncat(x);
		//return atd.findAttendmarkByIds(x);
	}
	
	@PostMapping("/attendencestudent")
	public List<Attendmark> studentAttendence(@RequestBody Student s)
	{
		
		int x  = s.getStudentid();
		return atd.oncat(x);
		//return atd.findAttendmarkByIds(x);
	}
	
	@PostMapping("/marksUpdate")
	public List<Attendmark> studentmarksUpdate(@RequestBody Attendmark atm)
	{
		int id = atm.getId();
		String subject = atm.getSubjectname();
		
		return atd.UpdateMark(id,subject);	 
	}
	
	
}
