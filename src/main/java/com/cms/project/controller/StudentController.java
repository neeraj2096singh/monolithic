
package com.cms.project.controller;

import java.util.List;
import java.util.Optional;

import org.aspectj.weaver.ast.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cms.project.Entity.Student;
import com.cms.project.reposistry.Studentreposistry;

import Service.StudentService;

@CrossOrigin("http://localhost:4200")
@RestController
@ComponentScan("Service")
public class StudentController {

	@Autowired
	private StudentService service ;
	
	
	 
	
	// runtime pe studentreposistry ka value springBoot inject kryega 
	/*@Autowired
	public void f1(Studentreposistry y)
	{ 
		System.out.println(" Student Reposistry autowired successfully ");
		studentreposistry =y;
	
	}*/
	
	// this is for student login
	@PostMapping("/studentlogin")
	public Student login(@RequestBody Student x)
	{
		return service.login(x);
	}
	
	//this is for adding new student
	@PostMapping("/newstudent")
	public Student insert(@RequestBody Student x)
	{
		return service.insert(x);
	}
	
	// this is for getting all studentg
	@PostMapping("/allstudent")
	public List<Student> findall()
	{
		return service.findall();
		
	}
	
	
	//this is for update student
	@PostMapping("/updateStudent")
	public Student updateStudent(@RequestBody Student v)
	{

		return service.updateStudent(v);
	}
	
	
	
	@PostMapping("/resetStudentPassword")
	public Student resetPassword(@RequestBody Student x)
	{
		return service.resetPassword(x);
	}
	
	
}
