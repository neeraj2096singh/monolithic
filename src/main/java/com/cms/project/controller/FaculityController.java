package com.cms.project.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms.project.Entity.Faculity;
import com.cms.project.Entity.Student;
import com.cms.project.reposistry.FaculityReposistry;

@CrossOrigin("http://localhost:4200")
@RestController
public class FaculityController {

	private FaculityReposistry faculityrepsosistry;
	
	
	@Autowired
	public void f1(FaculityReposistry y)
	{ 
		System.out.println("FaculityReposistry autowired successfully");
		faculityrepsosistry =y;
		
	}
	
	@PostMapping("/stafflogin")
	public Faculity login(@RequestBody Faculity x)
	{
		Faculity faculity = new Faculity();
		
		Optional<Faculity> oid= faculityrepsosistry.findById(x.getFacultyid());
		 if(oid.isPresent())
		 {
			 faculity = oid.get();
			 
			 if(faculity.getPassword().compareTo(x.getPassword())==0)
			 {
				 System.out.println(faculity.getFacultyname());
				  
				 x = faculity;
				
			 }
			
		 }
		
		return x;
	}
	
	@PostMapping("/newFaculity")
	public Faculity insert(@RequestBody Faculity x)
	{
		if(faculityrepsosistry.existsById(x.getFacultyid()))
		{	
			x.setFacultyid(0);
		
			
		}
		else
			
		{
			System.out.println("Elseeeeeeeee");
				faculityrepsosistry.save(x);
		}
		
		return x;
	}
	
	
	@PostMapping("/allFaculity")
	public List<Faculity> findall()
	{
		return faculityrepsosistry.findAll();
		
	}
	
	@PostMapping("/updateFaculity")
	public Faculity updateFaculity(@RequestBody Faculity v)
	{
		Faculity faculty = new Faculity();
		if(faculityrepsosistry.existsById(v.getFacultyid()))
		{
			faculty = faculityrepsosistry.findById(v.getFacultyid()).get();
			/*y isliye kiye because angular e hum password nhi bhej 
			rhe so direct save krne pe password null ho jaa rhaa thaa thats why humne phele 
			 uss id se single select krke student me dala aur whaa
			  se set password krke V me dalaa ayur save kiya*/
			
			
			v.setPassword(faculty.getPassword());
			faculityrepsosistry.save(v);			
			
		}
		else
		{
			v.setFacultyid(0);
		}
		return v;
	}
	
	@PostMapping("/resetFaculityPassword")
	public Faculity resetPassword(@RequestBody Faculity x)
	{
		Faculity faculty = new Faculity();
		
		Optional<Faculity> oid= faculityrepsosistry.findById(x.getFacultyid());
		 if(oid.isPresent())
		 {
			 faculty = oid.get();
			 
			 faculty.setPassword(x.getPassword());
			 
			 faculityrepsosistry.save(faculty);
			
		 }
		 
		return x;
	}
}
