package com.cms.project.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

@Component
@Entity
public class Student {
  
	@Id
	private int studentid;
	
	private String password;
	
	private String studentname;
	
	private String fathername;
	
	private String mothername;
	
	private String address;
	
	private String phoneno;
	
	private String email;
	
	private String branch;
	
	private String year;
	
	private String dob;

	public Student(int studentid, String password, String studentname, String fathername, String mothername,
			String address, String phoneno, String email, String branch, String year, String dob) {
		super();
		this.studentid = studentid;
		this.password = password;
		this.studentname = studentname;
		this.fathername = fathername;
		this.mothername = mothername;
		this.address = address;
		this.phoneno = phoneno;
		this.email = email;
		this.branch = branch;
		this.year = year;
		this.dob = dob;
	}

	
	
	public int getStudentid() {
		return studentid;
	}



	public void setStudentid(int studentid) {
		this.studentid = studentid;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getStudentname() {
		return studentname;
	}



	public void setStudentname(String studentname) {
		this.studentname = studentname;
	}



	public String getFathername() {
		return fathername;
	}



	public void setFathername(String fathername) {
		this.fathername = fathername;
	}



	public String getMothername() {
		return mothername;
	}



	public void setMothername(String mothername) {
		this.mothername = mothername;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getPhoneno() {
		return phoneno;
	}



	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getBranch() {
		return branch;
	}



	public void setBranch(String branch) {
		this.branch = branch;
	}



	public String getYear() {
		return year;
	}



	public void setYear(String year) {
		this.year = year;
	}



	public String getDob() {
		return dob;
	}



	public void setDob(String dob) {
		this.dob = dob;
	}



	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
