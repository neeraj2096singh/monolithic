package com.cms.project.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Timetable {
    
	@Id
	private String coursename;
	
	private int year;
	
	private int semester;
	
	private String subject;
	
    private String time;

	public Timetable(String coursename, int year, int semester, String subject, String time) {
		super();
		this.coursename = coursename;
		this.year = year;
		this.semester = semester;
		this.subject = subject;
		this.time = time;
	}

	public String getCoursename() {
		return coursename;
	}

	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getSemester() {
		return semester;
	}

	public void setSemester(int semester) {
		this.semester = semester;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Timetable() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    
}
