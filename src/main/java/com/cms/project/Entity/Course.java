package com.cms.project.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Course {
    @Id
	private String coursename;
	
	private int semester;
	
	private int year;
	
	

	public Course(String coursename, int semester, int year) {
		super();
		this.coursename = coursename;
		this.semester = semester;
		this.year = year;
	}



	public String getCoursename() {
		return coursename;
	}



	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}



	public int getSemester() {
		return semester;
	}



	public void setSemester(int semester) {
		this.semester = semester;
	}



	public int getYear() {
		return year;
	}



	public void setYear(int year) {
		this.year = year;
	}



	public Course() {
		super();
		// TODO Auto-generated constructor stub
	}



	@Override
	public String toString() {
		return "Course [coursename=" + coursename + ", semester=" + semester + ", year=" + year + "]";
	}
	
	
}
