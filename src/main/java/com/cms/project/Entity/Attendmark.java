package com.cms.project.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity

public class Attendmark {
    
	@Id
	//in AttendMark table no any primary key and we need to where id query so keep Subjectname is @Id so subjectName is not repeated 
	private String subjectname;
	@Column(name="studentid")
	private int id;
	
	
	private int mark;
	
	private int attendence;

	public Attendmark(int id, String subjectname, int mark, int attendence) {
		super();
		this.id = id;
		this.subjectname = subjectname;
		this.mark = mark;
		this.attendence = attendence;
	}

	

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getSubjectname() {
		return subjectname;
	}

	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	public int getAttendence() {
		return attendence;
	}

	public void setAttendence(int attendence) {
		this.attendence = attendence;
	}

	public Attendmark() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
