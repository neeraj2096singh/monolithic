package com.cms.project.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Faculity {

	@Id
	private int facultyid;
	
	private String password;
	
	private String facultyname;
	
	private String doj;
	
	private String address;
	
	private String phoneno;
	
	private String email;
	
	private String department;

	public Faculity(int facultyid, String password, String facultyname, String doj, String address, String phoneno,
			String email, String department) {
		super();
		this.facultyid = facultyid;
		this.password = password;
		this.facultyname = facultyname;
		this.doj = doj;
		this.address = address;
		this.phoneno = phoneno;
		this.email = email;
		this.department = department;
	}

	public int getFacultyid() {
		return facultyid;
	}

	public void setFacultyid(int facultyid) {
		this.facultyid = facultyid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFacultyname() {
		return facultyname;
	}

	public void setFacultyname(String facultyname) {
		this.facultyname = facultyname;
	}

	public String getDoj() {
		return doj;
	}

	public void setDoj(String doj) {
		this.doj = doj;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Faculity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
