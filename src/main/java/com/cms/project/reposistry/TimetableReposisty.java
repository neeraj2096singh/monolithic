package com.cms.project.reposistry;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cms.project.Entity.Attendmark;
import com.cms.project.Entity.Timetable;

public interface TimetableReposisty extends JpaRepository<Timetable,String>{
	@Query(" from Timetable where course =:course AND year =:year AND semester=:semester" )
	List<Timetable>TimetableDetail(@Param("course") String course,@Param("year") int year,@Param("semester") int semester);
}
