package com.cms.project.reposistry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cms.project.Entity.Course;

public interface CourseReposistry extends JpaRepository<Course, String> {

}
