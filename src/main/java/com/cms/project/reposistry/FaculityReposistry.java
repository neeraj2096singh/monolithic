package com.cms.project.reposistry;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cms.project.Entity.Faculity;




public interface FaculityReposistry extends JpaRepository<Faculity, Integer>{
	@Query("from Faculity where type = :x")
	List<Faculity> oncat(@Param("x") int y);
}
