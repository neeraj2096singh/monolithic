package com.cms.project.reposistry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cms.project.Entity.Admin;

public interface AdminReposistry extends JpaRepository<Admin, String> {

}
