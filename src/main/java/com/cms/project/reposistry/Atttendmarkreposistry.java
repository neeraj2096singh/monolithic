package com.cms.project.reposistry;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.cms.project.Entity.Attendmark;



@CrossOrigin("/http://localhost:4200")

public interface Atttendmarkreposistry extends JpaRepository<Attendmark, String> {

	@Query("from Attendmark where studentid = :x")
	//List<Attendmark> oncat(@Param("x") int y);
	//	@Query("FROM Laptop WHERE brand = :brand")
	List<Attendmark> oncat (int x);
//	public List<Attendmark> findAttendmarkByIds(@Param("ids") int ids);
	@Query(value = "SELECT * FROM Attendmark WHERE studentid = :x",
            nativeQuery = true)
	List<Attendmark> findMarkById(int x);

	
	@Query(" from Attendmark where studentid =:id AND subjectname =:subject" )
	List<Attendmark>UpdateMark(@Param("id") int id,@Param("subject") String subject);

}
