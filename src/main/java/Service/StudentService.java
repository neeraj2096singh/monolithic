package Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


import com.cms.project.Entity.Student;
import com.cms.project.reposistry.Studentreposistry;

	@Service
	@Component
public class StudentService {



		@Autowired
		private Studentreposistry studentreposistry;
		
		  @Autowired
		  private Student student;
		 
		
		// runtime pe studentreposistry ka value springBoot inject kryega 
		/*@Autowired
		public void f1(Studentreposistry y)
		{ 
			System.out.println(" Student Reposistry autowired successfully ");
			studentreposistry =y;
		
		}*/
		
		// this is for student login
		
		public Student login(Student x)
		{
			
			
			Optional<Student> oid= studentreposistry.findById(x.getStudentid());
			 if(oid.isPresent())
			 {
				
				 student = oid.get();
				 
				 if(student.getPassword().compareTo(x.getPassword())==0)
				 {
					 System.out.println(student.getStudentname());
					  
					 x = student;
					
				 }
				 else
				 {
					 x.setPassword("0");
				 }
				
			 }
			 System.out.println(student.getStudentname()+x.getPassword()+"  "+student.getPassword());
			return x;
		}
		
		//this is for adding new student
		
		public Student insert( Student x)
		{
			if(studentreposistry.existsById(x.getStudentid()))
			{	
				x.setStudentid(0);
			
				
			}
			else
				
			{
				
					studentreposistry.save(x);
			}
			
			return x;
		}
		
		// this is for getting all studentg
		
		public List<Student> findall()
		{
			return studentreposistry.findAll();
			
		}
		
		
		//this is for update student
		
		public Student updateStudent(Student v)
		{
		
			if(studentreposistry.existsById(v.getStudentid()))
			{
				student = studentreposistry.findById(v.getStudentid()).get();
				/*y isliye kiye because angular e hum password nhi bhej 
				rhe so direct save krne pe password null ho jaa rhaa thaa thats why humne phele 
				 uss id se single select krke student me dala aur whaa
				  se set password krke V me dalaa ayur save kiya*/
				
				
				v.setPassword(student.getPassword());
				studentreposistry.save(v);			
				
			}
			else
			{
				v.setStudentid(0);
			}
			return v;
		}
		
		
		
		
		public Student resetPassword( Student x)
		{
			
			
			Optional<Student> oid= studentreposistry.findById(x.getStudentid());
			 if(oid.isPresent())
			 {
				 student = oid.get();
				 
				 student.setPassword(x.getPassword());
				 
				 studentreposistry.save(student);
				
			 }
			 
			return x;
		}
		
		
	}
